<?php

use Core\App;
use Core\Container;
use Core\Database;

$container = new Container();

$container->bind('core\Database', function (){
    $config = require base_path("config.php");
    return new Database($config['database'], $config['auth']['username'], $config['auth']['password']);
});

App::setContainer($container);

//$db = $container->resolve ('Core\Database');

