<?php

use core\App;
use core\Database;

$db = App::resolve(Database::class);
dd($db);

$currentUserId = 1;


$note = $db->query(/** @lang text */ 'SELECT * from notes where id = :id',
    [
        'id' => $_GET['id']
    ]
)->findAndFail();
authorize($note['user_id'] === $currentUserId);

$db->query(/** @lang text */ 'DELETE FROM notes WHERE id = :id', [
    'id' => $_GET['id']
]);
header('location: /notes');
exit();
