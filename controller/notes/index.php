<?php

use core\App;
use core\Database;

$config = require base_path("config.php");
$notes = [];

$db = App::resolve(Database::class);

$notes = $db->query(/** @lang text */ 'SELECT * from notes')->get();
//dd($statement);

view("notes/notes.view.php", [
    "heading" => "My notes",
    "notes" => $notes
]);
