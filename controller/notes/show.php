<?php

use core\App;
use core\Database;

$db = App::resolve(Database::class);
$currentUserId = 1;

$note = $db->query(/** @lang text */ 'SELECT * from notes where id = :id',
    [
        'id' => $_GET['id']
    ]
)->findAndFail();
authorize($note['user_id'] === $currentUserId);

view("notes/note.view.php", [
    "heading" => "Note",
    "note" => $note
]);

