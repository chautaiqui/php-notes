<?php

use core\Validator;
use core\App;
use core\Database;

$db = App::resolve(Database::class);

$errors = [];

if (Validator::string($_POST['body'], 1, 1000)) {
    $errors['body'] = "A body of can be more than 1000 characters is required";
}

if (!empty($errors)) {
    view("notes/note-create.view.php", [
        "heading" => "Create a Note",
        "errors" => $errors
    ]);
}

$db->query(/** @lang text */ "INSERT INTO notes(body, user_id) VALUES (:body, :user_id)", [
    'body' => $_POST['body'],
    'user_id' => 1
]);

header ("location: /notes");
die();

