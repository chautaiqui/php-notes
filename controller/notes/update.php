<?php

use core\App;
use core\Validator;
use core\Database;

$db = App::resolve(Database::class);
$currentUserId = 1;

$note = $db->query(/** @lang text */ 'SELECT * from notes where id = :id',
    [
        'id' => $_POST['id']
    ]
)->findAndFail();

authorize($note['user_id'] === $currentUserId);

$errors = [];

if (Validator::string($_POST['body'], 1, 1000)) {
    $errors['body'] = "A body of can be more than 1000 characters is required";
}

if (count($errors)) {
    view("notes/note-edit.view.php", [
        "heading" => "Edit Note",
        "errors" => $errors,
        "note" => $note,
    ]);
}

$db->query( /** @lang text */ "UPDATE notes SET body = :body WHERE id = :id", [
    'body' => $_POST['body'],
    'id' => $_POST['id']
]);

header('location: /notes');
die();
