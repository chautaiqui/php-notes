<?php

use core\Validator;
use core\App;
use core\Database;

$email = $_POST['email'];
$password = $_POST['password'];

// validate forms
$errors = [];
if (!Validator::email($email)) {
    $errors['email'] = "Please provide a valid email.";
}

if (!Validator::string($password, 7, 255)) {
    $errors['password'] = "Please provide a valid password at least 7 characters.";
}
if (! empty($errors)) {
    view('/registration/create.view.php', [
        'heading' => 'Register',
        'errors' => $errors
    ]);
}
// check if account exists
$db = App::resolve(Database::class);

$user = $db->query( /** @lang text */ 'SELECT * from users WHERE email = :email', [
    'email' => $email
])->find();
//dd($user);
if ($user) {
    header ('location: /');
} else {
    $db->query( /** @lang text */ 'INSERT INTO users(email, password) values (:email, :password)', [
       'email' => $email,
       'password' => $password
    ]);

//    session_start();

    $_SESSION['user'] = [
        'email' => $email
    ];
    header ('location: /');
    exit();
}

 // yes -> login
 // create to datebase
