<?php

namespace Core;
use PDO;
use PDOException;
class Database {
    public PDO $connection;
    public $statement;
    public function  __construct ($config, $username = 'root', $password = '') {
        try {
            $dsn = "mysql:" . http_build_query($config, "", ";");
            $this->connection = new PDO($dsn, $username, $password);
            // set the PDO error mode to exception
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        } catch(PDOException $e) {
            echo $e;
        }
    }

    public function query ($query, $param = []): static
    {
        $this->statement = $this->connection->prepare($query);
        $this->statement->execute($param);
        return $this;
    }

    public function find () {
        return $this->statement->fetch();
    }

    public function get () {
        return $this->statement->fetchAll();
    }
    public function findAndFail () {
        $result = $this->find();

        if (! $result) {
            abort();
        }

        return $result;

    }


}
