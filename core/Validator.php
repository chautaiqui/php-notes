<?php

namespace Core;

class Validator
{
    public static function string($str, $min = 1, $max = INF): bool
    {
        return strlen(trim($str)) > $min && strlen(trim($str)) <= $max;
    }

    public static function email($email): bool
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

}
