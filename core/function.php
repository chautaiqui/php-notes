<?php
use core\Response;
function isUrl ($value): bool
{
    return $_SERVER['REQUEST_URI'] === $value;
}

function dd ($value): void
{
    echo "<pre>";
    var_dump($value);
    echo "</pre>";
//    die($value);
}

function abort ($code = 404) {
    http_response_code ( $code );
    require base_path ( "views/$code.php" );
    die();
}

function authorize ($condition, $status = Response::FORBIDDEN): void
{
    if (! $condition) {
        abort($status);
    }
}

function base_path ($path): string
{
    return BASE_PATH . $path;
}

function view ($path, $attributes = []): void
{

    extract($attributes);

    require base_path('views/' . $path);
}
