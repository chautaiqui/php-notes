<?php

$router->get('/', 'controller/index.php');

$router->get('/about', 'controller/about.php');

$router->get('/notes', 'controller/notes/index.php');
$router->post('/notes', 'controller/notes/store.php');

$router->get('/note', 'controller/notes/show.php');
$router->get('/note/edit', 'controller/notes/edit.php');
$router->patch('/note', 'controller/notes/update.php');
$router->delete('/note', 'controller/notes/destroy.php');

$router->get('/notes/create', 'controller/notes/create.php');
$router->post('/notes/create', 'controller/notes/create.php');

$router->get('/register', 'controller/registration/create.php');
$router->post('/register', 'controller/registration/store.php');
